import org.apache.jena.graph.Node;
import org.apache.jena.reasoner.rulesys.RuleContext;

// abstract this to make operations
public abstract class RDFext_dataAdapterOperation {
    // choose a name for this operation
    public abstract String Name();

    // the args, length and context are the same as provided to the rule call
    // ([0]: operation name, [1]: data name)
    // any additional nodes are arguments for this operation
    // the adapter will provide the data object
    // the return value is the rules return value
    public abstract boolean DoOperation(Node[] args, int length, RuleContext context, Object data);
}
