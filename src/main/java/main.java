import org.apache.jena.rdf.model.*;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;

import tech.tablesaw.api.ColumnType;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;

import javax.swing.*;
import java.awt.*;
import java.io.Console;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class main {

    public static GraphDisplayForm display;

    public static RDFext_dataAdapter dataAdapter;
    // example table
    private static String fetchThisTable = "https://raw.githubusercontent.com/jtablesaw/tablesaw/master/data/bush.csv";

    public static void makeJFrame(String name, JPanel contentPane)
    {
        JFrame frame = new JFrame(name);
        frame.setContentPane(contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        /*
        JFrame frame = new JFrame("ezui");
        ui = new ezui();
        frame.setContentPane(ui.contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        */


        display = new GraphDisplayForm("graph");

        makeJFrame("graph", display.contentPane);

        runRFDStuff();

    }

    public static void registerMyBuiltin() {
        // register my (probably broken) custom builtin
        BuiltinRegistry.theRegistry.register(new RDFext_areAll());

        // register data adapter and keep a reference to add operations and data
        dataAdapter = new RDFext_dataAdapter();
        BuiltinRegistry.theRegistry.register(dataAdapter);

        // register my data adapter operations
        dataAdapter.RegisterOperation(new TS_summarize());

        // make and register an example data set
        ColumnType[] columnTypes = {
                ColumnType.LOCAL_DATE, // 0 date
                ColumnType.SHORT,  // 1 approval
                ColumnType.STRING,   // 2 who
        };
        Table table;
        try{
            table = Table.read().usingOptions(CsvReadOptions.builder(new URL(fetchThisTable))
                    .tableName("bush")
                    .columnTypes(columnTypes));
        }
        catch(Exception e){
            System.out.println("could not access " + fetchThisTable + " " + e.toString());
            return;
        }
        dataAdapter.RegisterData(fetchThisTable, table);
    }

    public static void runRFDStuff()
    {
        // instead of putting data back and overwriting, I use the datasets "/in" and "/out"
        String connString = "http://192.168.17.167:3030/";
        boolean useMyServer = false;

        Model model;
        if(useMyServer){
            // load from my server
            RDFConnectionFuseki rdfConnectionIn = RDFConnectionFactory.connectFuseki(connString + "in");
            model = rdfConnectionIn.fetch();
        }
        else{
            // create manually
            model = ModelFactory.createDefaultModel();
            //model.setNsPrefix("my", "http://my.org/resource/");

            //<editor-fold desc="feed model some data">

            ArrayList<Statement> statementList = new ArrayList<Statement>();


            Resource r0 = ResourceFactory.createResource("r0_notlit");
            Resource r1 = ResourceFactory.createResource("r1_notlit");
            Resource r2 = ResourceFactory.createResource("r2_lit");
            Resource r3 = ResourceFactory.createResource("r3_lit");
            Resource r4 = ResourceFactory.createResource("r4-01");
            Resource r5 = ResourceFactory.createResource("r5-02");
            Resource r6 = ResourceFactory.createResource("r6-23");
            Resource r7 = ResourceFactory.createResource("r7-456");
            Resource r8 = ResourceFactory.createResource("r8-56");
            Resource r9 = ResourceFactory.createResource("r9-46");
            Resource r10 = ResourceFactory.createResource("r10-236");

            Property p_is = ResourceFactory.createProperty("is");
            Property p_isNot = ResourceFactory.createProperty("isNot");
            Property p_has = ResourceFactory.createProperty("has");

            Literal l_lit_af = ResourceFactory.createPlainLiteral("lit af");

            // resources which are lit af
            statementList.add(ResourceFactory.createStatement(r2,p_is,l_lit_af));
            statementList.add(ResourceFactory.createStatement(r3,p_is,l_lit_af));

            // resources which are not lit af
            statementList.add(ResourceFactory.createStatement(r0,p_isNot,l_lit_af));
            statementList.add(ResourceFactory.createStatement(r1,p_isNot,l_lit_af));

            // link up with "has" predicates

            // r4-01
            statementList.add(ResourceFactory.createStatement(r4,p_has,r0));
            statementList.add(ResourceFactory.createStatement(r4,p_has,r1));
            // r5-02
            statementList.add(ResourceFactory.createStatement(r5,p_has,r0));
            statementList.add(ResourceFactory.createStatement(r5,p_has,r2));
            // r6-23
            statementList.add(ResourceFactory.createStatement(r6,p_has,r2));
            statementList.add(ResourceFactory.createStatement(r6,p_has,r3));
            // r7-456
            statementList.add(ResourceFactory.createStatement(r7,p_has,r4));
            statementList.add(ResourceFactory.createStatement(r7,p_has,r5));
            statementList.add(ResourceFactory.createStatement(r7,p_has,r6));
            // r8-56
            statementList.add(ResourceFactory.createStatement(r8,p_has,r5));
            statementList.add(ResourceFactory.createStatement(r8,p_has,r6));
            // r9-46
            statementList.add(ResourceFactory.createStatement(r9,p_has,r4));
            statementList.add(ResourceFactory.createStatement(r9,p_has,r6));
            // r10-236
            statementList.add(ResourceFactory.createStatement(r10,p_has,r2));
            statementList.add(ResourceFactory.createStatement(r10,p_has,r3));
            statementList.add(ResourceFactory.createStatement(r10,p_has,r6));



            //</editor-fold>

            // data for dataAdapter tests

            Property p_hasTable = ResourceFactory.createProperty("hasTable");
            var tableName = ResourceFactory.createResource(fetchThisTable);
            statementList.add(ResourceFactory.createStatement(r0, p_hasTable, tableName));


            // add to model
            model.add(statementList);
        }

        registerMyBuiltin();

        String rules = //"@prefix my: <http://my.org/resource/>\n" +
                // rules to propagate "is" and "isNot" upwards through the nodes connected by "has"
                // since this seems to work left to right, simple statements go first, to prevent
                // my builtin from working when it doesn't need to
                "[rule_is:      (?a has ?b), (?b is ?something),    noValue(?a isNot ?something), areAll(?a, has, is, ?something),    -> (?a is ?something)]" +
                "[rule_isNot:   (?a has ?b), (?b isNot ?something), noValue(?a is ?something),    areAll(?a, has, isNot, ?something), -> (?a isNot ?something)]" +
                //"[rule_is: (?a has ?b), (?b is ?something), noValue(?a isNot ?something), (?a has ?c), noValue(?c isNot ?something) -> (?a is ?something)]" +
                //"[rule_isNot: (?a has ?b), (?b isNot ?something), noValue(?a is ?something), (?a has ?c), noValue(?c is ?something)  -> (?a isNot ?something)]"

                // update any af abbreviation
                // drop() will not fire rules, but re-adding should do so
                // remove() would lose the values and not re-add the triple with updated values
                "[rule_updateLiteralRemoveBadWords: (?a ?b ?bad), isLiteral(?bad), regex(?bad,'(.*)af', ?good) -> drop(0), (?a, ?b, ?good)]" +

                // test table summarization
                // will summarize if it cannot find the mean value
                "[summarizeAllTables: (?a hasTable ?t), noValue(?t mean ?x), data(TS_summarize, ?t, approval, ?mean, ?sum, ?min, ?max) ->" +
                    "(?t mean ?mean), (?t sum ?sum), (?t min ?min), (?t max ?max)]"
            ;

        // drop or remove will apply directly to the model, while re-adding adds into the InfModel, so make a copy first to compare later
        Model originalModel = ModelFactory.createDefaultModel().add(model);

        Reasoner reasoner = new GenericRuleReasoner(Rule.parseRules(rules));
        reasoner.setDerivationLogging(true);
        InfModel infModel = ModelFactory.createInfModel(reasoner, model);

        display.showModel(originalModel, infModel);
        //display.showModel(originalModel);

        // upload new data to fuseki server
        if(useMyServer) {
            RDFConnectionFuseki rdfConnectionOut = RDFConnectionFactory.connectFuseki(connString + "out");
            rdfConnectionOut.put(infModel);
        }


        // I used this once to put input data into fuseki
        /*
        RDFConnectionFuseki rdfConnectionIn = RDFConnectionFactory.connectFuseki(connString + "in");
        rdfConnectionIn.put(originalModel);
        */
    }


}
