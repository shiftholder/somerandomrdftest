import org.apache.jena.graph.Node;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

// created by niklas rogmann
// as should be obvious by how useless this is:
// searches for all objects in a given resource [0] for a given predicate [1]
// returns true,
// if all those objects in turn have another given predicate [2], pointing at another given object [3]
// can be used to upwards-fill with a property along nodes
public class RDFext_areAll extends BaseBuiltin
{
    @Override
    public String getName() {
        return "areAll";
    }

    @Override
    public int getArgLength() {
        return 4;
    }

    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        // args[]:
        // [0]: search this resource
        // [1]: for this predicate
        // [2]: whose resulting resource all have this predicate
        // [3]: in turn having this resource/literal
        // returns true if all resulting subject have the predicate-object pair given by 2,3


        var graph = context.getGraph();
        for(var iter1 = graph.find(args[0],args[1],Node.ANY); iter1.hasNext();){
            Node node1 = iter1.next().getObject();

            var iter3 = graph.find(node1,args[2], args[3]);
            if(!iter3.hasNext())
            {
                iter3.close();
                iter1.close();

                //System.out.println("checking " + args[0].toString() + " " + args[1].toString() + " " + node1.toString() + " NO " + args[2].toString() + " " + args[3].toString());

                return false;
            }
            else
            {
                //System.out.println("checking " + args[0].toString() + " " + args[1].toString() + " " + node1.toString() + " YES " + args[2].toString() + " " + args[3].toString());

            }
            iter3.close();
        }

        //System.out.println("checking " + args[0].toString() + " " + args[1].toString() + " are all " + args[2].toString() + " " + args[3].toString());

        return true;
    }
}