import org.apache.jena.graph.Node;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ezui {
    public JPanel contentPane;
    private JEditorPane editorPane1;
    private JPanel test;

    //<editor-fold desc="my UI functions">

    // set text in textbox (will clear anything previously)
    public void showText(String out){editorPane1.setText(out);}

    //</editor-fold>

    public static String ModelAsString(Model model, String newLinePreambles)
    {
        String string = "";

        // note: iterators will only release resources on the last entry
        // when not iteration through all of them, it needs to be closed manually

        {
            string += "\n" + newLinePreambles + "Subjects:\n\n";
            var iter = model.listSubjects();
            while (iter.hasNext()) {
                var r = iter.next();
                string += newLinePreambles + "\t" + r.toString() + "\n";
            }
        }
        {
            string += "\n" + newLinePreambles + "Statements:\n\n";
            var iter = model.listStatements();
            while (iter.hasNext()) {
                var r = iter.next();
                string += newLinePreambles + "\t" + r.toString() + "\n";
            }
        }
        /*{
            string += "\n" + newLinePreambles + "Objects:\n\n";
            var iter = model.listObjects();
            while (iter.hasNext()) {
                var r = iter.next();
                string += newLinePreambles + "\t" + r.toString() + "\n";
            }
        }*/

        string += "";
        return string;
    }


    public ezui(InfModel infModel) {

        // show input and output
        showText(
                "\ninput:\n" +
                ModelAsString(infModel.getRawModel(), "\t") +
                "\noutput:\n" +
                ModelAsString(infModel.getDeductionsModel(), "\t")
        );
    }

}
