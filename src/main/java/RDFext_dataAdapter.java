import org.apache.jena.graph.Node;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

import java.util.HashMap;
import java.util.Map;

// created by niklas rogmann
//
// can register arbitrary objects as data sources
// can register abstracted RDFext_dataAdapter.Operation classes that do something with data sources
//
// using this rule will then run a specified operation on a specified data source ( if both are registered )
// nodes and rule context are also supplied, such that variables can be bound
//
// rule returns false when either operation or data is not found



public class RDFext_dataAdapter extends BaseBuiltin {

    // data objects denoted by their names
    // names should preferably be URIs
    public Map<String, Object> dataSources = new HashMap<String, Object>();
    // operations denoted by their names
    public Map<String, RDFext_dataAdapterOperation> operations = new HashMap<String, RDFext_dataAdapterOperation>();

    // shortcut for adding data
    public void RegisterData(String name, Object newData){
        dataSources.put(name, newData);
    }

    // shortcut for adding an operation
    public void RegisterOperation(RDFext_dataAdapterOperation newOperation){
        operations.put(newOperation.Name(), newOperation);
    }

    @Override
    public String getName() {
        return "data";
    }

    @Override
    public int getArgLength() {
        return 0;
    } // minimum 2!

    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        // args[]:
        // [0]: name/URI of the operation to run
        // [1]: name/URI of the data to run the operation on
        if(length < 2)
            return false;
        if(!(CanNodeBeAKey(args[0]) && CanNodeBeAKey(args[1])))
            return false;
        String operationName = args[0].toString(false);
        String dataSourceName = args[1].toString(false);
        if (!(operations.containsKey(operationName) && dataSources.containsKey(dataSourceName)))
            return false;
        RDFext_dataAdapterOperation operation = operations.get(operationName);
        Object data = dataSources.get(dataSourceName);

        return operation.DoOperation(args, length, context, data);
    }

    private boolean CanNodeBeAKey(Node node){
        return (node.isLiteral() || node.isVariable() || node.isURI());
    }

}
