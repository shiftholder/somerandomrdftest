// demonstration of dataAdapter operations

import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.reasoner.rulesys.RuleContext;
import tech.tablesaw.api.Table;
import static tech.tablesaw.aggregate.AggregateFunctions.*;

public class TS_summarize extends RDFext_dataAdapterOperation{

    @Override
    public String Name() {
        return "TS_summarize";
    }

    @Override
    public boolean DoOperation(Node[] args, int length, RuleContext context, Object data) {
        Table table = (Table) data;
        if(table == null)
            return false;
        if(length < 2+1+4)
            return false;

        // [2]: column name

        // [3]: mean
        // [4]: sum
        // [5]: min
        // [6]: max

        Table summary = table.summarize(args[2].toString(), mean, sum, min, max).apply();

        int i = 0;
        int o = 3;
        context.getEnv().bind(args[i+o], NodeFactory.createLiteral(summary.column(i).get(0).toString()));
        i++;
        context.getEnv().bind(args[i+o], NodeFactory.createLiteral(summary.column(i).get(0).toString()));
        i++;
        context.getEnv().bind(args[i+o], NodeFactory.createLiteral(summary.column(i).get(0).toString()));
        i++;
        context.getEnv().bind(args[i+o], NodeFactory.createLiteral(summary.column(i).get(0).toString()));

        return true;
    }
}