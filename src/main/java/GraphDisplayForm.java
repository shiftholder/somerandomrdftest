import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraphDisplayForm {
    public JPanel contentPane;
    private JPanel controlPane;
    private JPanel viewPane;
    private JButton buttonShowIn;
    private JButton buttonShowOut;
    private JLabel label;
    private JButton buttonShowDelta;

    private RDFtoGraphStreamDisplay graphStreamDisplay;

    // can take either an infModel for before/after, or take two models to compare
    private InfModel infModel;
    private Model inModel, outModel;

    public GraphDisplayForm(String namedId) {
        graphStreamDisplay = new RDFtoGraphStreamDisplay(namedId);
        viewPane.add(graphStreamDisplay.getAwtComponent());

        buttonShowIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("In");
                if(infModel != null){
                    graphStreamDisplay.morphModel(infModel.getRawModel());
                }
                else if(inModel != null && outModel != null){
                    graphStreamDisplay.morphModel(inModel);
                }
            }
        });

        buttonShowOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("Out");
                if(infModel != null){
                    graphStreamDisplay.morphModel(infModel);
                }
                else if(inModel != null && outModel != null){
                    graphStreamDisplay.morphModel(outModel);
                }
            }
        });

        buttonShowDelta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("Delta");
                if(infModel != null){
                    graphStreamDisplay.morphModel(infModel.getDeductionsModel());
                }
                /*else if(model1 != null && model2 != null){
                   cannot do this without deductionsModel
                }*/
            }
        });

    }

    public void showModel(Model model) {
        infModel = null;
        inModel = null;
        outModel = model;
        showInit(model);
    }

    public void showModel(InfModel model) {
        infModel = model;
        inModel = null;
        outModel = null;
        showInit(model);
    }

    public void showModel(Model setModelIn, Model setModelOut){
        infModel = null;
        inModel = setModelIn;
        outModel = setModelOut;
        showInit(outModel);
    }

    private void showInit(Model model){
        graphStreamDisplay.showModel(model);
        boolean hasInfModel = infModel != null;
        boolean hasTwoModels = inModel != null;
        boolean hasComparable = hasInfModel || hasTwoModels;
        buttonShowDelta.setEnabled(hasInfModel);    // disable delta when not an InfModel
        buttonShowIn.setEnabled(hasComparable);     // disable in view when no comparing possible
        buttonShowOut.setEnabled(hasComparable);    // disable in view when no comparing possible
        if(hasComparable)
            label.setText("Out");
        else
            label.setText("-");
    }

}
