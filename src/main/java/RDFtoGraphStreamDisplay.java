import org.apache.commons.lang3.NotImplementedException;
import org.apache.jena.rdf.model.*;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.layout.Layouts;
import org.graphstream.ui.swing_viewer.SwingViewer;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import java.awt.*;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class RDFtoGraphStreamDisplay {

    private Graph graph;
    private Viewer viewer;
    private View view;
    private Layout layoutAlg;

    //<editor-fold desc="style sheet strings">

    // TODO: make style classes addable from outside

    public String style = "" +
            // general style
            "graph { " +
            "   fill-color: #000; " +
            "}" +
            "node { " +
            "   shape: box;" +
            "   fill-color: #1110;" +
            "   size: 100, 50;" +
            "   stroke-mode: none;" +
            //"   stroke-width: 1px;" +
            //"   stroke-color: #FFF;" +
            "   text-alignment: center;" +
            "   text-color: #FFF;" +
            "   text-size: 20px;" +
            "   text-background-mode: plain;" +
            "   text-background-color: #111;" +
            "   " +
            "}" +
            "edge {" +
            "   fill-color: #555;" +
            "   text-alignment: center;" +
            "   text-color: #AAA;" +
            "   text-size: 20px;" +
            "   text-background-mode: plain;" +
            "   text-background-color: #000;" +
            "   arrow-size: 20px, 8px;" +
            //"   shape: blob;" +
            "}" +
            // general class styles
            "node.literal {" +
            "   shape: box;" +
            "   fill-color: #6620;" +
            //"   size: 100, 50;" +
            "   stroke-mode: none;" +
            "   stroke-width: 2px;" +
            "   stroke-color: #FFF0;" +
            "   text-alignment: center;" +
            "   text-color: #FF5;" +
            "   text-size: 16px;" +
            "   text-background-mode: plain;" +
            "   text-background-color: #662;" +
            "}" +
            // custom class styles
            "edge.is {" +
            "   fill-color: #0F0;" +
            "   text-alignment: center;" +
            "   text-color: #8F8;" +
            "   text-size: 12px;" +
            "   text-background-color: #020;" +
            "}" +
            "edge.isNot {" +
            "   fill-color: #F00;" +
            "   text-alignment: center;" +
            "   text-color: #F88;" +
            "   text-size: 12px;" +
            "   text-background-color: #200;" +
            "}" +
            "";



    //</editor-fold>



    public RDFtoGraphStreamDisplay(String namedId) {
        System.setProperty("org.graphstream.ui", "swing");
        graph = new SingleGraph(namedId);
        viewer = new SwingViewer(graph,Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        view = viewer.addDefaultView(false);
    }

    // add statement as an edge into the graph
    private void addEdge(Statement statement){
        // need to avoid duplicate names with literals
        boolean objectIsLiteral = statement.getObject().isLiteral();
        // target node gets the entire statement as its id, if it is a literal. a _ is added to have the name different from the edge.
        String objectNameWorkaround = objectIsLiteral ? statement.toString() + " _ " : statement.getObject().toString();
        Edge newEdge = graph.addEdge(statement.toString(),statement.getSubject().toString(),objectNameWorkaround, true);
        if(newEdge == null){
            System.out.println("seems to happen for an edge where another edge already exists");
            return;
        }
        Node sourceNode = newEdge.getSourceNode();
        Node targetNode = newEdge.getTargetNode();
        // set labels
        String edgeClass = statement.getPredicate().toString();
        newEdge.setAttribute("ui.label", edgeClass);
        sourceNode.setAttribute("ui.label", statement.getSubject().toString());
        targetNode.setAttribute("ui.label", statement.getObject().toString());
        // set classes
        newEdge.setAttribute("ui.class", edgeClass);
        // TODO: source node class
        targetNode.setAttribute("ui.class", objectIsLiteral ? "literal" : objectNameWorkaround);
    }

    /*private String SplitLongNames(String name){
        var ssp = name.split("/#");
        return ssp[ssp.length-1];
    }*/

    // shows a new model, removing anything previous
    public void showModel(Model model)
    {
        graph.clear();
        graph.setStrict(false);
        graph.setAutoCreate(true);


        StmtIterator stmtIterator = model.listStatements();

        for(int i = 0;stmtIterator.hasNext();i++){
            Statement statement = stmtIterator.next();
            addEdge(statement);
        }

        graph.setAttribute("ui.stylesheet", style);
        layoutAlg = Layouts.newLayoutAlgorithm();
        viewer.enableAutoLayout(layoutAlg);

    }

    // change model to another model
    // will attempt to keep the same nodes and edges
    // will remove missing edges
    // will update values
    // will add new edges
    public void morphModel(Model model)
    {

        // remove edges from the graph, which are in the graph, but not in the model

        // the edges() iterator will not work while manipulating the graph
        // thus all edges will be buffered in an array
        Edge[] edges = graph.edges().toArray(Edge[]::new);
        for(int iEdge = 0; iEdge < edges.length; iEdge++){
            Edge edge = edges[iEdge];
            Node sourceNode = edge.getSourceNode();
            Node targetNode = edge.getTargetNode();
            boolean targetIsLiteral = ((String)targetNode.getAttribute("ui.class")).equals("literal");
            Resource resourceToSearch = ResourceFactory.createResource((String) sourceNode.getAttribute("ui.label"));
            Property propertyToSearch = ResourceFactory.createProperty((String) edge.getAttribute("ui.label"));
            var nodeIterator = model.listObjectsOfProperty(resourceToSearch, propertyToSearch);
            boolean nodeFound = false;
            while(nodeIterator.hasNext()){
                RDFNode rdfNode = nodeIterator.next();
                boolean targetShouldBeLiteral = rdfNode.isLiteral();
                if(targetShouldBeLiteral){
                    if(targetIsLiteral){
                        // we do not care about updating literals here, so they will be skipped
                        // it is assumed this is the correct literal, since it is under the same predicate
                        // this might make problems with duplicate predicates
                        nodeFound = true;
                        break;
                    }
                    else{
                        // resource is a literal, but this isn't, so this is not the same edge.
                        // if this actually is the same edge, it will be added by the next step.
                    }
                }
                else{
                    if(targetIsLiteral){
                        // resource is not a literal, but this is, so this is not the same edge.
                        // if this actually is the same edge, it will be added by the next step.
                    }
                    else{
                        // both are objects, so see if they are the same
                        if(rdfNode.toString().equals((String)targetNode.getAttribute("ui.label"))){
                            nodeFound = true;
                            break;
                        }
                    }
                }
            }
            if(!nodeFound) {
                if(targetIsLiteral) {
                    // literal nodes can only have a single edge pointing towards it
                    // this should remove node and edge
                    graph.removeNode(targetNode);
                }
                else{
                    // TODO: check if this leads to a proper representation
                    if(sourceNode.edges().count() == 1){
                        graph.removeNode(sourceNode);
                    }
                    else{
                        graph.removeEdge(edge);
                    }
                }
            }
        }


        // add edges to the graph, which are in the model, but not in the graph
        // TODO: for visual purposes, this could be made into a second step, with the user able to watch the transition

        StmtIterator stmtIterator = model.listStatements();
        int i = 0;
        while(stmtIterator.hasNext()) {
            i++;
            Statement statement = stmtIterator.next();
            boolean targetShouldBeLiteral = statement.getObject().isLiteral();

            String nodeToSearchFor = statement.getSubject().toString();
            boolean foundEdge = false;
            Node sourceNode = graph.getNode(nodeToSearchFor);
            if (sourceNode != null){
                var leavingEdges = sourceNode.leavingEdges().iterator();
                while(leavingEdges.hasNext()) {
                    Edge edge = leavingEdges.next();
                    String edgeLabel = (String) edge.getAttribute("ui.label");
                    if(Objects.equals(edgeLabel, statement.getPredicate().toString())){
                        Node targetNode = edge.getTargetNode();
                        boolean targetIsLiteral = ((String)targetNode.getAttribute("ui.class")).equals("literal");
                        if(targetShouldBeLiteral){
                            if(targetIsLiteral){
                                // is, and should be literal, simply update value
                                // it is assumed this is the correct literal, since it is under the same predicate
                                // this might make problems with duplicate predicates
                                targetNode.setAttribute("ui.label", statement.getObject().toString());
                                foundEdge = true;
                                break;
                            }
                            else{
                                //TODO: rdf model says its a literal, but the graph has this bound to a non-literal
                                throw new NotImplementedException("TODO");
                            }
                        }
                        else{
                            if(targetIsLiteral){
                                //TODO: rdf model says its not a literal, but the graph has this bound to a literal
                                throw new NotImplementedException("TODO");
                            }
                            else{
                                if(Objects.equals((String) targetNode.getAttribute("ui.label"), statement.getObject().toString())){
                                    foundEdge = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if(!foundEdge){
                // did not find an edge representing the statement in the graph,
                // thus it has to be added
                // to keep id unique, here a _ is added at the right hand side of the string,
                // unlike the left hand side as the showModel() call makes on
                addEdge(statement);
            }
        }


        // force keep calculating
        double oldStabLimit = layoutAlg.getStabilizationLimit();
        layoutAlg.setStabilizationLimit(999999999);
        // gets stuck after some hence and forth, so I shake it
        layoutAlg.shake();
        // make one frame
        layoutAlg.compute();
        layoutAlg.setStabilizationLimit(oldStabLimit);
    }

    // component to add into a form
    public Component getAwtComponent() {
        return (Component) view;
    }
}
